package debugging;

import java.util.Arrays;
import java.util.Scanner;
/**
 * Программа по поиску порядкового номера числа в рандомном массиве целых чисел
 *
 * @author Merzlyakov Iliya 18it18
 */

public class Sequentialsearch {
    private static final int MAX =  51;
    private static final int MIN = -25;
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите размер массива");
        int size = scanner.nextInt();
        int[] array = new int[size];
        randomElements(array);
        System.out.println(Arrays.toString(array));
        System.out.println("Введите число для поиска");
        int number = scanner.nextInt();
        int result = searchArrayElement(array, number);
        System.out.println((result == -1) ? "Нет такого значения." : "Номер элемента в последовательности: " + result);
    }
    
    /**
     * Метод, реализующий поиск порядкового номера элемента массива
     *
     * @param array
     * @param number
     */
    private static int searchArrayElement(int[] array, int number) {
        int index = -1;
        for (int i = 0; i < array.length; i++) {
            if (array[i] == number) {
                index = i;
            }
        }
        if (index == -1) {
            return 0;
        } else {
            return index+1;
        }
    }

    /**
     * Метод, заполняющий массив рандомными элементами
     *
     * @param array
     */
    private static void randomElements(int[] array) {
        for (int i = 0; i < array.length; i++) {
            array[i] = -25 + (int) (Math.random() * 51);
        }
    }
}